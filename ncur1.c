#include <linux/limits.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <algorithm>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <ncurses.h>
#include <panel.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <vector>

int NLINES = 40; 
int NCOLS = 35; // count of lines and columns
int COUNT[2];
std::vector<std::string> curdir(2); // current directory
std::vector<std::vector<char*> > files_list(2); // files in current directory
int numwin; // current window
std::string filename[2];

bool cmpr(const char* c1, const char* c2) {
    return (strcmp(c1, c2) < 0);
}

void init_list(WINDOW *win, int y, int x, int curwin) { // init files list in current directory
    int count = 0; struct dirent* pdir;
    DIR* dr = opendir(curdir[curwin].data());
    while ((pdir = readdir(dr)) != NULL) {        
        files_list[curwin].push_back(pdir->d_name);
        ++count;
    } 
    std::sort(files_list[curwin].begin(), files_list[curwin].end(), cmpr);
    COUNT[curwin] = count;
}
 
void print_list(WINDOW *win, int width, char *string, chtype color, int curwin) { // print files list
    int x = 0, y = 0; 
    getyx(win, y, x);      
    if(width == 0) { width = 80; } 
    wattron(win, color);
    mvwprintw(win, y + 1, x + 10, "%s", string);  
    wattroff(win, color);
    if (files_list[curwin].empty()) { init_list(win, y, x, curwin); }
    for (int idx = 0; idx < COUNT[curwin]; ++idx) {
        mvwprintw(win, y + idx + 3, x + 1, "%s", files_list[curwin][idx]);
    }
    refresh();
}

void win_show(WINDOW *win, char *label, int curwin) { // start a window
    int startx, starty, height, width;
    getbegyx(win, starty, startx);
    getmaxyx(win, height, width);
    box(win, 0, 0);
    print_list(win, width, label, COLOR_PAIR(1), curwin);
    wmove(win, 4, 1);
}

void init_wins(WINDOW **wins) { // init both windows
    int x = 0, y = 0;
    char label[80];
    for(int i = 0; i < 2; ++i) {
        wins[i] = newwin(NLINES, NCOLS, y, x + NCOLS * i);
	    sprintf(label, "Window %d", i);
	    win_show(wins[i], label, i);
	    x += 5;
    }
}


int main() {
    struct winsize w;
    ioctl(0, TIOCGWINSZ, &w);
    NLINES = w.ws_row - 1;
    NCOLS = int((w.ws_col - 10) / 2);
    memset(&w, 0, sizeof(w));
    curdir[0] = "."; curdir[1] = ".";
    WINDOW *my_wins[2];
    PANEL *my_panels[2];
    PANEL *top;
    initscr();
    start_color();
    cbreak(); noecho();
    keypad(stdscr, TRUE);
    init_pair(1, COLOR_YELLOW, COLOR_BLACK);
    int ch; numwin = 1;
    struct stat try_dir;
    char workdir[PATH_MAX + 1];
    getcwd(workdir, PATH_MAX + 1);
    curdir[0] = workdir;
    curdir[1] = workdir;
    start:
    init_wins(my_wins);  
    my_panels[0] = new_panel(my_wins[0]);
    my_panels[1] = new_panel(my_wins[1]);
    set_panel_userptr(my_panels[0], my_panels[1]);
    set_panel_userptr(my_panels[1], my_panels[0]);
    update_panels(); doupdate();
    top = my_panels[numwin];
    if (numwin == 0) {
        top_panel(top);
        update_panels(); doupdate();
    }
    int xx, yy; // cursor position
    wmove(my_wins[numwin], 4, 1);
    while((ch = getch()) != 'Q') {
        switch(ch) {
            case 9: { // To other window
	        top = (PANEL *)panel_userptr(top);
 	        top_panel(top);
                numwin = 1 - numwin;
                wmove(my_wins[numwin], 4, 1);
                break;
	        }
            case KEY_UP: { // Up 
                xx = getcurx(my_wins[numwin]);
                yy = getcury(my_wins[numwin]);
                if (yy >= 4) { wmove(my_wins[numwin], yy - 1, 1); }
                break; 
            }
            case KEY_DOWN: { // Down 
                xx = getcurx(my_wins[numwin]);
                yy = getcury(my_wins[numwin]);
                if (yy <= COUNT[numwin] + 1) { wmove(my_wins[numwin], yy + 1, 1); }
                break; 
            }
            case 10: { // Enter - open directory
                xx = getcurx(my_wins[numwin]);
                yy = getcury(my_wins[numwin]); 
                if (strcmp(files_list[numwin][yy - 3], ".") == 0) { break; }
                filename[numwin] = curdir[numwin];
                filename[numwin] +=  "/"; 
                filename[numwin] += files_list[numwin][yy - 3];
                stat(filename[numwin].data(), &try_dir);
                if (S_ISDIR(try_dir.st_mode)) {
                    if (strcmp(files_list[numwin][yy - 3], "..") == 0) {
                        int pos = curdir[numwin].find_last_of('/');
                        curdir[numwin] = curdir[numwin].substr(0, pos);
                        if (curdir[numwin] == "") { curdir[numwin] = "/"; }
                    } else {
                        if (curdir[numwin] != "/") {
                            curdir[numwin] +=  "/"; 
                        }
                        curdir[numwin] += files_list[numwin][yy - 3];
                    }
                    files_list[numwin].clear();
                    update_panels(); doupdate();
                    goto start;
                } 
                break;
            }
        }
	    update_panels(); doupdate();
    }
    endwin();
    memset(&try_dir, 0, sizeof(try_dir));
    return 0;
}